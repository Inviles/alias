import React from 'react';
import PropTypes from 'prop-types';

import classes from './Modal.module.scss';
import Backdrop from '../Backdrop/Backdrop';

const Modal = ({ isShown, onClose, children }) => {
  let modalClasses = [classes.modal];

  if (isShown) {
    modalClasses = [...modalClasses, classes.modal_status_shown];
  }

  return (
    <>
      <Backdrop isShown={isShown} onClick={onClose} />
      <div className={modalClasses.join(' ')}>
        {children}
      </div>
    </>
  );
};

Modal.propTypes = {
  isShown: PropTypes.bool.isRequired,
  onClose: PropTypes.func,
  children: PropTypes.node.isRequired,
};

Modal.defaultProps = {
  onClose: () => {},
};

export default Modal;
