import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';

import classes from './Switch.module.scss';

const Switch = ({ isEnabled, onChange }) => {
  const [isSwitchEnabled, changeIsSwitchEnabled] = useState(isEnabled);

  const toggleSwitch = useCallback((event) => {
    const { target: { checked } } = event;

    changeIsSwitchEnabled(checked);
    onChange(checked);
  }, [onChange]);

  let siderClasses = [classes.switch__slider];

  if (isSwitchEnabled) {
    siderClasses = [...siderClasses, classes.switch__slider_status_enabled];
  }

  return (
    <label className={classes.switch}>
      <input
        className={classes.switch__input}
        type="checkbox"
        checked={isSwitchEnabled}
        onChange={toggleSwitch}
      />
      <span className={siderClasses.join(' ')} />
    </label>
  );
};

Switch.propTypes = {
  isEnabled: PropTypes.bool.isRequired,
  onChange: PropTypes.func,
};

Switch.defaultProps = {
  onChange: () => {},
};

export default Switch;
