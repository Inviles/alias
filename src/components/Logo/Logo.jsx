import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import classes from './Logo.module.scss';

const Logo = ({ className, hideTextOnMobile }) => {
  let textClasses = [classes.logo__text];

  if (hideTextOnMobile) {
    textClasses = [classes.logo__text, classes.logo__text_mobile_hidden];
  }

  return (
    <div className={[classes.logo, className].join(' ')}>
      <div className={classes.logo__circle}>
        <FontAwesomeIcon icon="dice" />
      </div>
      <p className={textClasses.join(' ')}>Alias</p>
    </div>
  );
};

Logo.propTypes = {
  className: PropTypes.string,
  hideTextOnMobile: PropTypes.bool,
};

Logo.defaultProps = {
  className: '',
  hideTextOnMobile: false,
};

export default Logo;
