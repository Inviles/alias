import React, { useState, useRef, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import PageTitle from '../PageTitle/PageTitle';
import Input from '../Input/Input';
import Button from '../Button/Button';
import classes from './Teams.module.scss';
import * as actions from '../../store/actions';

const maxAmountOfTeams = 7;

const Teams = ({ history }) => {
  const teams = useSelector((state) => state.teams);

  const [isButtonDisabled, changeIsButtonDisabled] = useState(false);
  const [validationErrors, changeValidationErrors] = useState([]);

  const dispatch = useDispatch();
  const timerIdRef = useRef(null);

  const onChangeInput = useCallback((id, value) => {
    changeIsButtonDisabled(true);
    let errors = [...validationErrors];

    clearTimeout(timerIdRef.current);

    if (!value) {
      errors.push(id);
    } else {
      errors = errors.filter((error) => error !== id);
    }

    timerIdRef.current = setTimeout(() => {
      dispatch(actions.updateTeamName(id, value));
      changeValidationErrors(errors);

      if (!errors.length) {
        changeIsButtonDisabled(false);
      }
    }, 500);
  }, [dispatch, validationErrors]);

  const addTeam = useCallback(() => {
    if (teams.length >= maxAmountOfTeams) {
      return;
    }

    changeIsButtonDisabled(true);
    dispatch(actions.addTeam());
  }, [dispatch, teams]);

  const validateFields = useCallback(() => {
    const errors = [];

    teams.forEach((team) => {
      if (team.name === '') {
        errors.push(team.id);
      }
    });

    changeValidationErrors(errors);

    return errors.length > 0;
  }, [teams]);

  const startPlaying = useCallback(() => {
    const isErrorsExist = validateFields();

    if (isErrorsExist) {
      return;
    }

    dispatch(actions.startGame());
    history.push('/game');
  }, [dispatch, history, validateFields]);

  return (
    <div className={classes.teams}>
      <PageTitle>Set up teams</PageTitle>
      <form className={classes.teams__list}>
        {teams.map((team) => (
          <Input
            key={team.id}
            className={classes.teams__input}
            leftIcon="circle"
            leftIconColor={team.color}
            rightIcon="times"
            rightIconColor="crimson"
            rightIconClick={() => dispatch(actions.deleteTeam(team.id))}
            value={team.name}
            error={validationErrors.includes(team.id) ? 'A team has to be specified' : null}
            onChange={(value) => onChangeInput(team.id, value)}
          />
        ))}
        <button type="button" className={classes.teams__button} onClick={addTeam}>
          <FontAwesomeIcon icon="plus" />
          <span className={classes['teams__button-text']}>Add team</span>
        </button>
      </form>
      <div className={classes['teams__nav-buttons']}>
        <Button
          btnType="secondary"
          onClick={history.goBack}
        >
          Back
        </Button>
        <Button
          onClick={startPlaying}
          isDisabled={isButtonDisabled}
        >
          Play
        </Button>
      </div>
    </div>
  );
};

Teams.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
    goBack: PropTypes.func,
  }).isRequired,
};

export default Teams;
