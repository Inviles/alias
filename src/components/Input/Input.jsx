import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import classes from './Input.module.scss';

const Input = ({
  id, isFocused, value, error, placeholder, onChange, className, leftIcon, leftIconColor,
  rightIcon, rightIconColor, leftIconClick, rightIconClick,
}) => {
  const [inputState, changeInputState] = useState({
    isFocused,
    value,
    error,
  });

  useEffect(() => {
    changeInputState((prevInputState) => ({
      ...prevInputState,
      error,
    }));
  }, [error]);

  const onInputChange = (event, onChangeProp) => {
    const { target: { value: targetValue } } = event;

    changeInputState((prevState) => ({
      ...prevState,
      error: '',
      value: targetValue,
    }));

    onChangeProp(targetValue);
  };

  const changeFocusedState = (bool) => {
    changeInputState((prevState) => ({
      ...prevState,
      isFocused: bool,
    }));
  };

  let inputClasses = [classes.input, className];
  let barClasses = [classes.input__bar];
  let errorNode = null;
  const icons = [];

  if (inputState.isFocused) {
    inputClasses = [...inputClasses, classes.input_state_focused];
    barClasses = [...barClasses, classes.input__bar_state_focused];
  }

  if (inputState.error) {
    inputClasses = [...inputClasses, classes.input_state_error];
    barClasses = [...barClasses, classes.input__bar_state_error];
    errorNode = <span className={classes.input__error}>{inputState.error}</span>;
  }

  if (leftIcon) {
    inputClasses = [...inputClasses, classes['input_left-icon_visible']];

    icons.push(
      <FontAwesomeIcon
        key={Math.random()}
        className={[classes.input__icon, classes.input__icon_type_left].join(' ')}
        icon={leftIcon}
        color={leftIconColor}
        onClick={leftIconClick}
      />,
    );
  }

  if (rightIcon) {
    inputClasses = [...inputClasses, classes['input_right-icon_visible']];

    icons.push(
      <FontAwesomeIcon
        key={Math.random()}
        className={[classes.input__icon, classes.input__icon_type_right].join(' ')}
        icon={rightIcon}
        color={rightIconColor}
        onClick={rightIconClick}
      />,
    );
  }

  return (
    <div className={inputClasses.join(' ')}>
      {icons}
      <input
        id={id || null}
        className={classes.input__field}
        type="text"
        value={inputState.value}
        placeholder={placeholder}
        onChange={(event) => onInputChange(event, onChange)}
        onFocus={() => changeFocusedState(true)}
        onBlur={() => changeFocusedState(false)}
      />
      <span className={barClasses.join(' ')} />
      {errorNode}
    </div>
  );
};

export default Input;

Input.propTypes = {
  id: PropTypes.string,
  isFocused: PropTypes.bool,
  value: PropTypes.string,
  error: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  leftIcon: PropTypes.string,
  leftIconColor: PropTypes.string,
  rightIcon: PropTypes.string,
  rightIconColor: PropTypes.string,
  leftIconClick: PropTypes.func,
  rightIconClick: PropTypes.func,
  onChange: PropTypes.func,
};

Input.defaultProps = {
  id: '',
  isFocused: false,
  value: '',
  error: '',
  placeholder: '',
  className: '',
  leftIcon: '',
  leftIconColor: '',
  rightIcon: '',
  rightIconColor: '',
  leftIconClick: () => {},
  rightIconClick: () => {},
  onChange: () => {},
};
