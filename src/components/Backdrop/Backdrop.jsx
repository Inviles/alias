import React from 'react';
import PropTypes from 'prop-types';

import classes from './Backdrop.module.scss';

const Backdrop = ({ isShown, onClick }) => {
  let backdropClasses = [classes.backdrop];

  if (isShown) {
    backdropClasses = [classes.backdrop, classes.backdrop_status_shown];
  }

  return <div className={backdropClasses.join(' ')} onClick={onClick} />;
};

Backdrop.propTypes = {
  isShown: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Backdrop;
