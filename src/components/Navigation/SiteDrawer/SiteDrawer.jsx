import React from 'react';
import PropTypes from 'prop-types';

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Backdrop from '../../Backdrop/Backdrop';
import DrawerToggler from './DrawerToggler/DrawerToggler';
import classes from './SiteDrawer.module.scss';

const SiteDrawer = ({ isShown, toggleSiteDrawer }) => {
  let siteDrawerClasses = [classes['site-drawer']];

  if (isShown) {
    siteDrawerClasses = [classes['site-drawer'], classes['site-drawer_state_visible']];
  }

  return (
    <>
      <Backdrop
        isShown={isShown}
        onClick={toggleSiteDrawer}
      />
      <div className={siteDrawerClasses.join(' ')}>
        <div className={classes['site-drawer__wrapper']}>
          <Logo />
          <DrawerToggler isActive={isShown} toggle={toggleSiteDrawer} size="small" />
        </div>
        <NavigationItems className={classes['site-drawer__navigation-items']} />
      </div>
    </>
  );
};

SiteDrawer.propTypes = {
  isShown: PropTypes.bool.isRequired,
  toggleSiteDrawer: PropTypes.func.isRequired,
};

export default SiteDrawer;
