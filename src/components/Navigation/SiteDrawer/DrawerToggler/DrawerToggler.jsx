/*
  <DrawerToggler
    size="normal | small"          // default: primary
    isActive                       // drawer state
    toggle={clickHandler}          // required
  />
*/


import React from 'react';
import PropTypes from 'prop-types';

import classes from './DrawerToggler.module.scss';

const DrawerToggler = ({ toggle, isActive, size }) => {
  let drawerClasses = [
    classes['drawer-toggler'],
    classes[`drawer-toggler_size_${size}`],
  ];
  let drawerItemClasses = [
    classes['drawer-toggler__item'],
    classes[`drawer-toggler__item_size_${size}`],
  ];

  if (isActive) {
    drawerClasses = [...drawerClasses, classes['drawer-toggler_state_opened']];
    drawerItemClasses = [...drawerItemClasses, classes['drawer-toggler__item_state_opened']];
  }

  return (
    <div
      className={drawerClasses.join(' ')}
      onClick={toggle}
    >
      <div className={drawerItemClasses.join(' ')} />
    </div>
  );
};

DrawerToggler.propTypes = {
  toggle: PropTypes.func.isRequired,
  isActive: PropTypes.bool,
  size: PropTypes.string,
};

DrawerToggler.defaultProps = {
  isActive: false,
  size: 'normal',
};

export default DrawerToggler;
