import React from 'react';
import PropTypes from 'prop-types';

import NavigationItems from '../NavigationItems/NavigationItems';
import Logo from '../../Logo/Logo';
import DrawerToggler from '../SiteDrawer/DrawerToggler/DrawerToggler';
import classes from './Toolbar.module.scss';

const Toolbar = ({ toggleSiteDrawer }) => (
  <header className={classes.toolbar}>
    <DrawerToggler toggle={toggleSiteDrawer} />
    <Logo
      className={classes.toolbar__logo}
      hideTextOnMobile
    />
    <NavigationItems className={classes['toolbar__navigation-items']} />
  </header>
);

Toolbar.propTypes = {
  toggleSiteDrawer: PropTypes.func.isRequired,
};

export default Toolbar;
