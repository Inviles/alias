import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import classes from './NavigationItem.module.scss';

const NavigationItem = ({
  children,
  exact,
  link,
  className,
}) => (
  <li className={[classes['navigation-item'], className].join(' ')}>
    <NavLink
      className={classes['navigation-item__link']}
      to={link}
      exact={exact}
      activeClassName={classes['navigation-item__link_status_active']}
    >
      {children}
    </NavLink>
  </li>
);

NavigationItem.propTypes = {
  children: PropTypes.node.isRequired,
  exact: PropTypes.bool,
  link: PropTypes.string.isRequired,
  className: PropTypes.string,
};

NavigationItem.defaultProps = {
  exact: false,
  className: '',
};

export default NavigationItem;
