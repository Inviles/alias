import React from 'react';
import PropTypes from 'prop-types';

import NavigationItem from './NavigationItem/NavigationItem';
import classes from './NavigationItems.module.scss';

const NavigationItems = ({ className }) => (
  <nav className={[classes['navigation-items'], className].join(' ')}>
    <ul className={classes['navigation-items__list']}>
      <NavigationItem className={classes['navigation-items__item']} link="/play">Play</NavigationItem>
      <NavigationItem className={classes['navigation-items__item']} link="/rules">Rules</NavigationItem>
      <NavigationItem className={classes['navigation-items__item']} link="/settings">Settings</NavigationItem>
      <NavigationItem className={classes['navigation-items__item']} link="/settings">Share</NavigationItem>
      <NavigationItem className={classes['navigation-items__item']} link="/settings">Feedback</NavigationItem>
    </ul>
  </nav>
);

NavigationItems.propTypes = {
  className: PropTypes.string,
};

NavigationItems.defaultProps = {
  className: '',
};

export default NavigationItems;
