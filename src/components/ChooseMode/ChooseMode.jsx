import React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import DescriptionCard from './DescriptionCard/DescriptionCard';
import PageTitle from '../PageTitle/PageTitle';
import classes from './ChooseMode.module.scss';
import * as actions from '../../store/actions';

const ChooseMode = ({ history }) => {
  const dispatch = useDispatch();

  const chooseSingleMode = () => {
    dispatch(actions.setSinglePlayerMode());
    history.push('/teams');
  };

  const chooseMultiMode = () => {
    dispatch(actions.setMultiPlayerMode());
    history.push('/teams');
  };

  const modes = [
    {
      title: 'One device',
      description: 'All the game continues on one device. When it’s another’s team turn you should path a device to them.',
      click: chooseSingleMode,
    },
    {
      title: 'Multiple devices',
      description: 'Each team has it’s own link. You create them game, share the links accordingly. Each player sees the whole game but see the words only it’s his turn.',
      click: chooseMultiMode,
    },
  ];

  return (
    <div className={classes.modes}>
      <PageTitle>Choose a mode</PageTitle>
      <ul className={classes.modes__list}>
        {modes.map((mode) => (
          <li key={mode.title} className={classes['modes__list-item']}>
            <DescriptionCard title={mode.title} description={mode.description} click={mode.click} />
          </li>
        ))}
      </ul>
    </div>
  );
};

ChooseMode.propTypes = {
  history: PropTypes.shape({ push: PropTypes.func }).isRequired,
};

export default ChooseMode;
