import React from 'react';
import PropTypes from 'prop-types';

import classes from './DescriptionCard.module.scss';

const DescriptionCard = ({ title, description, click }) => (
  <div className={classes['description-card']} onClick={click}>
    <strong className={classes['description-card__title']}>{title}</strong>
    <p className={classes['description-card__description']}>{description}</p>
  </div>
);

DescriptionCard.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  click: PropTypes.func,
};

DescriptionCard.defaultProps = {
  click: () => {},
};

export default DescriptionCard;
