import React, {
  useState, useEffect, useRef, useCallback,
} from 'react';
import PropTypes from 'prop-types';

import classes from './Timer.module.scss';

const STROKE_WIDTH = 3;
const BLUE = '#05f';
const LIGHT_GRAY = '#f5f5f5';
const GRAY = '#555555';

const Timer = ({
  isTimerStarted, time, size, className, onFinish,
}) => {
  const circleRadius = (size - STROKE_WIDTH) / 2;
  const circumference = circleRadius * 2 * Math.PI;
  const coordsCenter = size / 2;

  const [timer, changeTimer] = useState({
    progress: 0,
    time,
  });

  const timerIdRef = useRef(null);

  useEffect(() => {
    if (timer.time <= 0) {
      clearInterval(timerIdRef.current);
      onFinish();
    }
  }, [onFinish, timer.time]);

  const startTimer = useCallback(() => {
    timerIdRef.current = setInterval(() => {
      changeTimer((prevTimerState) => {
        const newTime = prevTimerState.time - 1;
        const progress = (time - newTime) / time;

        return {
          ...prevTimerState,
          time: newTime,
          progress,
        };
      });
    }, 1000);
  }, [time]);

  useEffect(() => {
    if (isTimerStarted) {
      return startTimer();
    }

    changeTimer({
      progress: 0,
      time,
    });
  }, [isTimerStarted, startTimer, time]);

  const getFormattedTime = useCallback(() => {
    let minutes = parseInt(timer.time / 60, 10);
    let seconds = parseInt(timer.time % 60, 10);

    minutes = minutes < 10
      ? `0${minutes}`
      : minutes;

    seconds = seconds < 10
      ? `0${seconds}`
      : seconds;

    return `${minutes}:${seconds}`;
  }, [timer.time]);

  const circleStyles = {
    strokeDashoffset: circumference - timer.progress * circumference,
    strokeDasharray: `${circumference} ${circumference}`,
  };

  return (
    <div className={[classes.timer, className].join(' ')}>
      <svg
        height={size}
        width={size}
      >
        <circle
          strokeWidth={STROKE_WIDTH}
          r={circleRadius}
          cx={coordsCenter}
          cy={coordsCenter}
          stroke={LIGHT_GRAY}
          fill="transparent"
        />
        <circle
          strokeWidth={STROKE_WIDTH}
          r={circleRadius}
          cx={coordsCenter}
          cy={coordsCenter}
          stroke={BLUE}
          style={circleStyles}
          className={classes['timer__progress-circle']}
          fill="transparent"
        />
        <text
          x="50%"
          y="50%"
          className={classes.timer__counter}
          textAnchor="middle"
          dominantBaseline="middle"
          fill={GRAY}
        >
          {getFormattedTime()}
        </text>
      </svg>
    </div>
  );
};

Timer.propTypes = {
  isTimerStarted: PropTypes.bool.isRequired,
  time: PropTypes.number,
  size: PropTypes.number,
  className: PropTypes.string,
  onFinish: PropTypes.func,
};

Timer.defaultProps = {
  time: 60,
  size: 100,
  className: '',
  onFinish: () => {},
};

export default Timer;
