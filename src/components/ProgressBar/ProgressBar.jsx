import React from 'react';
import PropTypes from 'prop-types';

import classes from './ProgressBar.module.scss';

const ProgressBar = ({ percent, isFullWidth, className }) => {
  let progressBarClasses = [classes['progress-bar'], className];

  if (isFullWidth) {
    progressBarClasses = [...progressBarClasses, classes['progress-bar_type_full-width']];
  }

  return (
    <div className={progressBarClasses.join(' ')}>
      <div className={classes['progress-bar__stat']} style={{ width: `${percent}%` }} />
    </div>
  );
};

export default ProgressBar;

ProgressBar.propTypes = {
  isFullWidth: PropTypes.bool,
  className: PropTypes.string,
  percent: PropTypes.number,
};

ProgressBar.defaultProps = {
  isFullWidth: false,
  className: '',
  percent: 0,
};
