import React from 'react';
import PropTypes from 'prop-types';

import classes from './PageTitle.module.scss';

const PageTitle = ({ children }) => (
  <>
    <h1 className={classes['page-title__text']}>{children}</h1>
    <hr size="1" className={classes['page-title__hr-line']} />
  </>
);

PageTitle.propTypes = {
  children: PropTypes.string.isRequired,
};

export default PageTitle;
