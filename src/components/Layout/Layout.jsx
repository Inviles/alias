import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Toolbar from '../Navigation/Toolbar/Toolbar';
import SiteDrawer from '../Navigation/SiteDrawer/SiteDrawer';
import classes from './Layout.module.scss';

const Layout = ({ children }) => {
  const [isSiteDrawerVisible, setIsSiteDrawerVisible] = useState(false);

  const toggleSiteDrawer = () => {
    setIsSiteDrawerVisible(!isSiteDrawerVisible);
  };

  return (
    <div className={classes.layout}>
      <Toolbar toggleSiteDrawer={toggleSiteDrawer} />
      <SiteDrawer isShown={isSiteDrawerVisible} toggleSiteDrawer={toggleSiteDrawer} />
      <main className={classes.layout__content}>
        {children}
      </main>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
