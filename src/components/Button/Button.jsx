/*
  <Button
    btnType="primary | secondary"  // default: primary
    className=""                   // custom button class
    onClick={clickHandler}         // required
    isDisabled                     // default: false
  />
*/

import React from 'react';
import PropTypes from 'prop-types';

import classes from './Button.module.scss';

const Button = ({
  children,
  onClick,
  isDisabled,
  btnType,
  className,
}) => (
  <button
    type="button"
    className={[classes.button, classes[`button_type_${btnType}`], className].join(' ')}
    onClick={onClick}
    disabled={isDisabled}
  >
    {children}
  </button>
);

Button.propTypes = {
  children: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool,
  btnType: PropTypes.string,
  className: PropTypes.string,
};

Button.defaultProps = {
  isDisabled: false,
  btnType: 'primary',
  className: '',
};

export default Button;
