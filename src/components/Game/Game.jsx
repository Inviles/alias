import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import classes from './Game.module.scss';
import GameProgress from './GameProgress/GameProgress';
import Timer from '../Timer/Timer';
import Button from '../Button/Button';
import Words from './Words/Words';
import * as actions from '../../store/actions';

const Game = () => {
  const { currentPlayingTeam, isRoundStarted } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [isTimerFinished, changeIsTimerFinished] = useState(false);

  useEffect(() => {
    if (isRoundStarted) {
      changeIsTimerFinished(false);
    }
  }, [isRoundStarted]);

  return (
    <div className={classes.game}>
      <GameProgress />
      <Timer
        time={5}
        isTimerStarted={isRoundStarted}
        className={classes.game__timer}
        onFinish={() => changeIsTimerFinished(true)}
      />
      { isRoundStarted
        ? <Words isTimerFinished={isTimerFinished} />
        : (
          <>
            <h1>{currentPlayingTeam.name}</h1>
            <p className={classes['game__starting-text']}>Are you ready to start?</p>
            <Button
              className={[classes.game__button, classes.game__button_type_start].join(' ')}
              onClick={() => dispatch(actions.startRound())}
            >
            Start
            </Button>
          </>
        )}
    </div>
  );
};

export default Game;
