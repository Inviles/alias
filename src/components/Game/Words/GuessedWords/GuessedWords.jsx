import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import classes from './GuessedWords.module.scss';
import Button from '../../../Button/Button';
import Switch from '../../../Switch/Switch';

const GuessedWords = ({
  passedWords, initiateNextRound, updateWordStatus, currentPlayingTeam,
}) => {
  const onSwitchChange = (wordInfo, newValue) => {
    const newWordInfo = {
      ...wordInfo,
      isGuessed: newValue,
    };

    updateWordStatus(newWordInfo);
  };

  return (
    <div className={classes['guessed-words']}>
      <h1>Guessed words:</h1>
      <ul className={classes['guessed-words__list']}>
        {passedWords.map((wordInfo) => (
          <li key={wordInfo.word}>
            <div className={classes['guessed-words__word-wrapper']}>
              <span className={classes['guessed-words__word']}>{wordInfo.word}</span>
              { wordInfo.guessedTeamId === currentPlayingTeam.id
                ? (
                  <Switch
                    isEnabled={wordInfo.isGuessed}
                    onChange={(newValue) => onSwitchChange(wordInfo, newValue)}
                  />
                )
                : (
                  <FontAwesomeIcon
                    className={classes['guessed-words__icon']}
                    icon="check"
                    size="2x"
                  />
                )}
            </div>
            <hr size="1" className={classes['guessed-words__hr-line']} />
          </li>
        ))}
      </ul>
      <Button
        className={classes['guessed-words__button']}
        onClick={initiateNextRound}
      >
          Done
      </Button>
    </div>
  );
};

GuessedWords.propTypes = {
  passedWords: PropTypes.arrayOf(
    PropTypes.shape({
      word: PropTypes.string.isRequired,
      isGuessed: PropTypes.bool.isRequired,
      guessedTeamId: PropTypes.number.isRequired,
    }),
  ).isRequired,

  initiateNextRound: PropTypes.func.isRequired,
  updateWordStatus: PropTypes.func.isRequired,

  currentPlayingTeam: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }).isRequired,
};

export default GuessedWords;
