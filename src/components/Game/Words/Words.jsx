import React, { useState, useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import classes from './Words.module.scss';
import Button from '../../Button/Button';
import Modal from '../../Modal/Modal';
import GuessedWords from './GuessedWords/GuessedWords';
import * as actions from '../../../store/actions';

const Words = ({ isTimerFinished }) => {
  const {
    words: wordsFromRedux,
    teams,
    currentPlayingTeam,
  } = useSelector((state) => state);

  const [words, changeWords] = useState({
    available: wordsFromRedux,
    passed: [],
    current: '',
  });

  const dispatch = useDispatch();
  const [isModalShown, changeIsModalShown] = useState(false);

  const getRandomWord = useCallback(() => {
    changeWords((previousWords) => {
      const availableWords = previousWords.available;

      if (availableWords.length === 0) {
        throw new Error('There are no words!');
      }

      const randomIndex = Math.floor(Math.random() * availableWords.length);
      const randomWord = availableWords[randomIndex];

      return {
        ...previousWords,
        available: previousWords.available.filter((word) => word !== randomWord),
        current: randomWord,
      };
    });
  }, []);

  const initiateNextRound = useCallback(() => {
    dispatch(actions.initiateNextRound(words.available, words.passed));
  }, [dispatch, words.available, words.passed]);

  useEffect(() => {
    getRandomWord();
  }, [getRandomWord]);

  const finishWord = useCallback((isGuessed, teamId) => {
    changeWords((previousWords) => ({
      ...previousWords,
      passed: [
        ...previousWords.passed,
        {
          word: previousWords.current,
          isGuessed,
          guessedTeamId: teamId,
        },
      ],
    }));

    if (isTimerFinished) {
      changeIsModalShown(true);
    } else {
      getRandomWord();
    }
  }, [getRandomWord, isTimerFinished]);

  const updateWordStatus = useCallback((newWordInfo) => {
    changeWords((previousWords) => {
      let updatedPassedWords = [...previousWords.passed];

      updatedPassedWords = updatedPassedWords.map((oldWordInfo) => {
        if (oldWordInfo.word === newWordInfo.word) {
          return newWordInfo;
        }

        return oldWordInfo;
      });

      return {
        ...previousWords,
        passed: updatedPassedWords,
      };
    });
  }, []);

  let bottomBontent = (
    <div className={classes['words__buttons-wrapper']}>
      <Button
        className={[classes.words__buton, classes.words__button_type_skip].join(' ')}
        onClick={() => finishWord(false, currentPlayingTeam.id)}
        btnType="secondary"
      >
          Skip
      </Button>
      <Button
        className={[classes.words__buton, classes.words__button_type_done].join(' ')}
        onClick={() => finishWord(true, currentPlayingTeam.id)}
      >
          Done
      </Button>
    </div>
  );

  if (isTimerFinished) {
    bottomBontent = (
      <ul className={classes.words__teams}>
        {teams.map((team) => (
          <li className={classes.words__team} key={team.id}>
            <div
              className={classes.words__circle}
              style={{ backgroundColor: team.color }}
              onClick={() => finishWord(true, team.id)}
            >
              <span className={classes.words__progress}>{team.progress}</span>
            </div>
            <span className={classes['words__team-name']}>{team.name}</span>
          </li>
        ))}
      </ul>
    );
  }

  return (
    <div className={classes.words}>
      <h1 className={classes.words__word}>{words.current}</h1>
      {bottomBontent}
      <Modal isShown={isModalShown}>
        <GuessedWords
          passedWords={words.passed}
          initiateNextRound={initiateNextRound}
          updateWordStatus={updateWordStatus}
          currentPlayingTeam={currentPlayingTeam}
        />
      </Modal>
    </div>
  );
};

Words.propTypes = {
  isTimerFinished: PropTypes.bool.isRequired,
};

export default Words;
