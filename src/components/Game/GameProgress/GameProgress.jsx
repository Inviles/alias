import React, { useCallback } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSelector } from 'react-redux';

import classes from './GameProgress.module.scss';
import ProgressBar from '../../ProgressBar/ProgressBar';

const GameProgress = () => {
  const { teams, goal } = useSelector((state) => state);

  const getProgressPercent = useCallback((progress, wordsGoal) => (progress * 100) / wordsGoal, []);

  return (
    <div className={classes['game-progress']}>
      <div className={classes['game-progress__track']}>
        <FontAwesomeIcon
          className={[classes['game-progress__icon'], classes['game-progress__icon_type_home']].join(' ')}
          icon="home"
          size="4x"
        />
        <div className={classes['game-progress__progress-wapper']}>
          {teams.map((team) => (
            <ProgressBar
              key={team.id}
              percent={getProgressPercent(team.progress, goal)}
              className={classes['game-progress__progress-line']}
              isFullWidth
            />
          ))}
        </div>
        <FontAwesomeIcon
          className={[classes['game-progress__icon'], classes['game-progress__icon_type_flag']].join(' ')}
          icon="flag-checkered"
          size="4x"
        />
      </div>
    </div>
  );
};

export default GameProgress;
