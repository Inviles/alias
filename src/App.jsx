import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import Layout from './components/Layout/Layout';
import ChooseMode from './components/ChooseMode/ChooseMode';
import Teams from './components/Teams/Teams';
import Game from './components/Game/Game';

const App = () => (
  <div>
    <Layout>
      <Switch>
        <Route path="/play" exact component={ChooseMode} />
        <Route path="/teams" exact component={Teams} />
        <Route path="/game" exact component={Game} />
        <Redirect to="/" />
      </Switch>
    </Layout>
  </div>
);

export default App;
