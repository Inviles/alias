import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faDice,
  faCircle,
  faPlus,
  faHome,
  faFlagCheckered,
  faTimes,
  faCheck,
} from '@fortawesome/free-solid-svg-icons';


library.add(
  faDice,
  faCircle,
  faPlus,
  faHome,
  faFlagCheckered,
  faTimes,
  faCheck,
);
