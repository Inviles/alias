export const SET_SIGNLE_PLAYER_MODE = 'SET_SIGNLE_PLAYER_MODE';
export const SET_MULTI_PLAYER_MODE = 'SET_MULTI_PLAYER_MODE';
export const CHANGE_TEAMS = 'CHANGE_TEAMS';
export const ADD_TEAM = 'ADD_TEAM';
export const DELETE_TEAM = 'DELETE_TEAM';
export const UPDATE_TEAM_NAME = 'UPDATE_TEAM_NAME';
export const START_GAME = 'START_GAME';
export const INITIATE_NEXT_ROUND = 'INITIATE_NEXT_ROUND';
export const START_ROUND = 'START_ROUND';
export const UPDATE_WORDS = 'UPDATE_WORDS';

export const setSinglePlayerMode = () => ({
  type: SET_SIGNLE_PLAYER_MODE,
});

export const setMultiPlayerMode = () => ({
  type: SET_MULTI_PLAYER_MODE,
});

export const changeTeams = (teams) => ({
  type: CHANGE_TEAMS,
  teams,
});

export const addTeam = () => ({
  type: ADD_TEAM,
});

export const deleteTeam = (id) => ({
  type: DELETE_TEAM,
  id,
});

export const updateTeamName = (id, name) => ({
  type: UPDATE_TEAM_NAME,
  id,
  name,
});

export const startGame = () => ({
  type: START_GAME,
});

export const initiateNextRound = (availableWords, passedWords) => ({
  type: INITIATE_NEXT_ROUND,
  availableWords,
  passedWords,
});

export const startRound = () => ({
  type: START_ROUND,
});
