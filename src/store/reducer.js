import * as actions from './actions';
import words from '../dicts/eng';

const initialState = {
  isSinglePlayer: false,
  isMultiPlayer: false,
  isGameStarted: false,
  isRoundStarted: false,
  currentPlayingTeam: {
    id: null,
    name: null,
  },
  currentPlayingPlayer: null,
  teams: [
    {
      id: Math.random(),
      name: 'Team Stark',
      color: 'crimson',
      progress: 0,
    },
    {
      id: Math.random(),
      name: 'Team Captain',
      color: 'darkorange',
      progress: 0,
    },
  ],
  words,
  goal: 50,
};

const teamColors = ['teal', 'orchid', 'purple', 'slateblue', 'limegreen'];

const addTeam = (state) => {
  const currentTeamColors = state.teams.map((team) => team.color);
  const newColor = teamColors.find((color) => !currentTeamColors.includes(color));

  const updatedTeams = [
    ...state.teams,
    {
      id: Math.random(),
      name: '',
      color: newColor,
      progress: 0,
    },
  ];

  return {
    ...state,
    teams: updatedTeams,
  };
};

const deleteTeam = (state, action) => ({
  ...state,
  teams: state.teams.filter((team) => team.id !== action.id),
});

const setSinglePlayerMode = (state) => ({
  ...state,
  isMultiPlayer: false,
  isSinglePlayer: true,
});

const setMultiPlayerMode = (state) => ({
  ...state,
  isMultiPlayer: true,
  isSinglePlayer: false,
});

const changeTeams = (state, action) => ({
  ...state,
  teams: action.teams,
});

const updateTeamName = (state, action) => {
  const updatedTeams = state.teams.map((team) => {
    if (team.id === action.id) {
      return {
        ...team,
        name: action.name,
      };
    }

    return team;
  });

  return {
    ...state,
    teams: updatedTeams,
  };
};

const startGame = (state) => ({
  ...state,
  isRoundStarted: false,
  isGameStarted: true,
  currentPlayingTeam: {
    id: state.teams[0].id,
    name: state.teams[0].name,
  },
  words,
});

const initiateNextRound = (state, action) => {
  let nextTeamIndex = state.teams.findIndex((team) => team.id === state.currentPlayingTeam.id) + 1;

  nextTeamIndex = nextTeamIndex >= state.teams.length
    ? 0
    : nextTeamIndex;

  const nextPlayingTeam = {
    id: state.teams[nextTeamIndex].id,
    name: state.teams[nextTeamIndex].name,
  };

  const teamGuessedLastWord = action.passedWords[action.passedWords.length - 1].guessedTeamId;

  const guessedWordsNumber = action.passedWords.reduce((acc, wordInfo) => {
    if (wordInfo.isGuessed && wordInfo.guessedTeamId === state.currentPlayingTeam.id) {
      return acc + 1;
    }

    return acc;
  }, 0);

  const updatedTeams = state.teams.map((team) => {
    const isGuessedLastWord = teamGuessedLastWord === team.id;
    const isCurrentPlayingTeam = state.currentPlayingTeam.id === team.id;
    let updatedProgressValue = team.progress;

    if (isCurrentPlayingTeam) {
      updatedProgressValue += guessedWordsNumber;
    } else if (isGuessedLastWord) {
      updatedProgressValue += 1;
    }

    return {
      ...team,
      progress: updatedProgressValue,
    };
  });

  return {
    ...state,
    teams: updatedTeams,
    currentPlayingTeam: nextPlayingTeam,
    words: action.availableWords,
    isRoundStarted: false,
  };
};

const startRound = (state) => ({
  ...state,
  isRoundStarted: true,
});

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_SIGNLE_PLAYER_MODE: return setSinglePlayerMode(state);
    case actions.SET_MULTI_PLAYER_MODE: return setMultiPlayerMode(state);
    case actions.CHANGE_TEAMS: return changeTeams(state, action);
    case actions.UPDATE_TEAM_NAME: return updateTeamName(state, action);
    case actions.ADD_TEAM: return addTeam(state);
    case actions.DELETE_TEAM: return deleteTeam(state, action);
    case actions.START_GAME: return startGame(state);
    case actions.INITIATE_NEXT_ROUND: return initiateNextRound(state, action);
    case actions.START_ROUND: return startRound(state);
    default: return state;
  }
};

export default reducer;
